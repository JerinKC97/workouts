#
#check color contrast for color combos
#

class Color
    attr_reader :r, :g, :b
    def initialize()
        print "Enter RGb color values followed by enter:"
        @r = gets.chop().to_i
        @g = gets.chop().to_i
        @b = gets.chop().to_i
    end

    def brightness_index
      return ( 299*@r + 587*@g + 114*@b) / 1000
    end
  
    def brightness_difference(another_color)
      return (brightness_index() - another_color.brightness_index()).abs
    end
  
    def hue_difference(another_color)
      return (@r - another_color.r).abs + (@g - another_color.g).abs + (@b - another_color.b).abs
    end
  
    def enough_contrast(another_color)
      if(brightness_difference(another_color) > 125 and hue_difference(another_color) > 500)
        puts "colors have sufficient contrast"
      else
        puts "colors does not have sufficient contrast"
      end
    end
end
  
color1 = Color.new()
color2 = Color.new()
color1.enough_contrast(color2)