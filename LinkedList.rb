#
#Implementation of linked list
#

class Node
    attr_accessor :value, :next
    def initialize(value)
        @value = value
        @next = nil
    end
end

class LinkedList
    def initialize
        @head = Node.new(nil)
        @numberOfItems = 0
    end
    def traverse()
        current = @head
        print("\tLIST : ")
        index=0
        print("   index => value  ")
        if(current != nil)
            while(current.value)
                print("|   #{index} => #{current.value}  ")
                current = current.next
                return 0 if(current == nil)
                index += 1
            end
            else
                @head = Node.new(nil)
        end
        puts ""
    end

    def push()
        current = @head
        flag = false #check the value whether already added or not
        print("Enter node value: ")
        value = gets.chop()
        @numberOfItems +=1
        if(@head.value == nil)
            @head.value = value
            flag = true 
        end
        while(current.next != nil and !flag)
            current = current.next
        end
        current.next = Node.new(value) if(!flag)
        puts "Value is added to the list"
        traverse()
    end

    def pop()
        traverse()
        current = @head
        print "\nEnter the index of value to be deleted: "
        index = gets.chop().to_i
        @numberOfItems -= 1 if (@numberOfItems > 0)
        if(index > @numberOfItems)
            puts "ENTER VALID INDEX!"
            pop()
        end
        if(index == 0)
            @head = @head.next
        end        
        for i in 0..@numberOfItems
            if(i+1 == index)
                current.next = current.next.next
            end
        end
        puts "\nValue is removed from the list.."
        traverse()
    end
end 

linkedList = LinkedList.new
loop do
    puts "\n===================================================="
    puts "1. Traverse\n2. Add Node\n3. Delete node\n4. Exit"
    print "Please select one of the choices (1, 2, 3, 4): "
    choice = gets.chop().to_i
    
    case choice
    when 1
        linkedList.traverse()
    when 2
        linkedList.push()
    when 3
        linkedList.pop()
    when 4
        puts "GOOD BYE:)"
        break
    else
        puts "INVALID CHOICE!"
    end
    break if(choice == 4)
end