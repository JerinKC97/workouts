#
#Distance between two mappoints
#

class MapPoint
    attr_accessor :lat, :lon

    def initialize
        print "Enter latitude: "
        @lat = gets.chop().to_f
        print "Enter longitude: "
        @lon = gets.chop().to_f
    end
end


puts "\nMapoint 1\n---------"
point1 = MapPoint.new
puts "\nMapPoint 2\n----------"
point2 = MapPoint.new

print point1.lat
dlon = point2.lon - point1.lon
dlat = point2.lat - point1.lat
a = (Math.sin(dlat/2))**2 + Math.cos(point1.lat) * Math.cos(point2.lat) * (Math.sin(dlon/2))**2
c = 2 * Math.atan2( Math.sqrt(a), Math.sqrt(1-a) )
d = 6373 * c

puts "\n Distance in KMs: #{d}\n"