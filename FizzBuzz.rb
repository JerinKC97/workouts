#
#Proram to print Fizz and Buzz accordingly
#

def checkMultiple(num)
    print "Fizz" if num%3 == 0
    print "Buzz" if num%5 == 0
end

(1..100).each{ |num|
    (num%3==0 or num%5==0) ? checkMultiple(num) : print("#{num}")
    print "\n"
}